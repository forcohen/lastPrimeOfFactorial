/* Algoritimo desenvolvido na aula de LP1 (linguagem de programação)
/  Autor: Arthur Cohen
/  Curso: Bacharel em Tenologia da Informacao - UFRN
/  04/03/2017
*/

#include <iostream>
#include "fatorial.h"
#include "primo.h"

using namespace std;

int main(void){
    cout<<"digite um numero natural:"<<endl;
    int k;
    cin >> k;
    k = fator(k); //recupera o fatorial do numero
    k = primoAnterior(k); //recupera o numero primo mais proximo a esquerda
    cout<<k<<endl;
    return 0;
}
