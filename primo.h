/* Algoritimo desenvolvido na aula de LP1 (linguagem de programação)
/  Autor: Arthur Cohen
/  Curso: Bacharel em Tenologia da Informacao - UFRN
/  04/03/2017
*/

#ifndef PRIMO_H
#define PRIMO_H

//define busca de um primo a esquerda de um k
int primoAnterior(int k);

//checa se um k e primo
bool isPrime(int k);

#endif
