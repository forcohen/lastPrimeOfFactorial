/* Algoritimo desenvolvido na aula de LP1 (linguagem de programação)
/  Autor: Arthur Cohen
/  Curso: Bacharel em Tenologia da Informacao - UFRN
/  04/03/2017
*/

#include "primo.h"

int primoAnterior(int k){
    for (int i = k-1; i>1; i--){
        if (isPrime(i)) return i; //se i for primo, retorna i
    }
    return -1; //saida padrao para numero sem primos antecessores
}


bool isPrime(int k){
    int count = 0;
    for (int i = 1; i<=k; i++){
        if ((k%i)==0) count++; //se divisivel por um i qualquer, incrementa
    }
    if (count==2) return true; //caso seja divisivel por apenas 2 numeros (1 e k), True
    return false;
}

