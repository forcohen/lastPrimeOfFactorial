#  Algoritimo desenvolvido na aula de LP1 (linguagem de programação)
#  Autor: Arthur Cohen
#  Curso: Bacharel em Tenologia da Informacao - UFRN
#  04/03/2017

CC=g++
CFLAGS=-Wall -pedantic
EXEC=exec

all: main clean

main: main.o fatorial.o primo.o
	$(CC) -o $(EXEC) main.o primo.o fatorial.o

primo.o: primo.cpp primo.h
	$(CC) -c primo.cpp primo.h $(CFLAGS)

fatorial.o: fatorial.cpp fatorial.h
	$(CC) -c fatorial.cpp fatorial.h $(CFLAGS)

main.o: main.cpp
	$(CC) -c main.cpp $(CFLAGS)

clean:
	rm -f *.o *.gch
