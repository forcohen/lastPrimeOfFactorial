/* Algoritimo desenvolvido na aula de LP1 (linguagem de programação)
/  Autor: Arthur Cohen
/  Curso: Bacharel em Tenologia da Informacao - UFRN
/  04/03/2017
*/

#include "fatorial.h"

int fator(int k){
    if (k < 0) return -1; //caso especial, numero negativo
    if (k <= 1) return 1; //caso especial, zero
    return (fator(k-1)*k); //base da recussao
}

